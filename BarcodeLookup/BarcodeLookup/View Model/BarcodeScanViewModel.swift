//
//  BarcodeScanViewModel.swift
//  BarcodeLookup
//
//  Created by John Douglas on 20/02/18.
//  Copyright © 2018 John Douglas. All rights reserved.
//

import Foundation

protocol BarcodeScanViewModel {
    func didCapture(code: String, ofType type: String)
    func errored(with error: Error)
    func scanningButtonTapped()
    func scanningCloseButtonTapped()
}

class BarcodeScanViewModelImpl: BarcodeScanViewModel {

    var barcodeLookupService: BarcodeLookupService!
    
    weak var view: BarcodeScanView?
    
    func didCapture(code: String, ofType type: String) {
        barcodeLookupService.lookup(barcode: code, onSuccess: { [weak self] (response) in
            guard let strongSelf = self else { return }
            strongSelf.view?.setProduct(name: response.name, by: response.brand)
        }) { [weak self] (error) in
            guard let strongSelf = self else { return }
            switch (error) {
                case NetworkerError.malformedResponse:
                    strongSelf.view?.setErrorMessage(message: "oops, something went wrong with that barcode")
                case NetworkerError.transportError:
                    strongSelf.view?.setErrorMessage(message: "hmm. we can't connect you to the lookup service. are you connected to WIFI?")
                case NetworkerError.unexpectedHTTPStatusCode:
                    strongSelf.view?.setErrorMessage(message: "oops, something went wrong.")
                case NetworkerError.unknownResponseFormat:
                    strongSelf.view?.setErrorMessage(message: "hmm. we didn't recognise that barcode")
                }
            }
    }
    
    func errored(with error: Error) {
        view?.setErrorMessage(message: error.localizedDescription)
    }
    
    func scanningButtonTapped() {
        view?.startScanning()
    }
    
    func scanningCloseButtonTapped() {
        view?.stopScanning()
    }
    
}
