//
//  BarcodeScanViewController.swift
//  BarcodeLookup
//
//  Created by John Douglas on 20/02/18.
//  Copyright © 2018 John Douglas. All rights reserved.
//

import BarcodeScanner
import UIKit

protocol BarcodeScanView: class {
    var viewModel: BarcodeScanViewModel! { get set }
    func startScanning()
    func stopScanning()
    func setErrorMessage(message: String)
    func setProduct(name: String, by brand: String)
}

final class BarcodeScanViewController: UIViewController, BarcodeScanView {
    
    private lazy var barcodeScannerVC: BarcodeScannerViewController = {
        let vc = BarcodeScannerViewController()
        vc.headerViewController.titleLabel.text = "Barcode Scanner"
        vc.headerViewController.closeButton.setTitle("close", for: .normal)
        vc.messageViewController.regularTintColor = .black
        vc.messageViewController.errorTintColor = .red
        vc.messageViewController.textLabel.textColor = .black
        vc.codeDelegate = self
        vc.errorDelegate = self
        vc.dismissalDelegate = self
        return vc
    }()

    var viewModel: BarcodeScanViewModel!
    
    func startScanning() {
        DispatchQueue.main.async { [weak self] in
            guard let strongSelf = self else { return }
            strongSelf.barcodeScannerVC.messageViewController.textLabel.text = "SCANNING"
            strongSelf.navigationController?.present(strongSelf.barcodeScannerVC, animated: true)
        }

    }
    
    func stopScanning() {
        DispatchQueue.main.async { [weak self] in
            guard let strongSelf = self else { return }
            strongSelf.barcodeScannerVC.dismiss(animated: true)
        }
    }
    
    func setErrorMessage(message: String) {
        DispatchQueue.main.async { [weak self] in
            guard let strongSelf = self else { return }
            strongSelf.barcodeScannerVC.resetWithError(message: message)
        }
    }
    
    func setProduct(name: String, by brand: String) {
        DispatchQueue.main.async { [weak self] in
            guard let strongSelf = self else { return }
            strongSelf.barcodeScannerVC.messageViewController.textLabel.text = "Found \(name) by brand (\(brand)" // yes, am aware I'm breaking demeters law. time-conscious hack.
            DispatchQueue.main.asyncAfter(deadline: .now() + 4, execute: { [weak self] in
                guard let strongSelf = self else { return }
                strongSelf.barcodeScannerVC.reset(animated: true)
            })
        }

    }
    
    @IBAction private func scanningButtonTapped(_ sender: Any) {
        viewModel.scanningButtonTapped()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

extension BarcodeScanViewController: BarcodeScannerCodeDelegate {
    
    func scanner(_ controller: BarcodeScannerViewController, didCaptureCode code: String, type: String) {
        controller.messageViewController.textLabel.text = "PROCESSING"
        viewModel.didCapture(code: code, ofType: type)
    }
    
}

extension BarcodeScanViewController: BarcodeScannerErrorDelegate {
    
    func scanner(_ controller: BarcodeScannerViewController, didReceiveError error: Error) {
        viewModel.errored(with: error)
    }
    
}

extension BarcodeScanViewController: BarcodeScannerDismissalDelegate {
    
    func scannerDidDismiss(_ controller: BarcodeScannerViewController) {
        viewModel.scanningCloseButtonTapped()
    }
    
}
