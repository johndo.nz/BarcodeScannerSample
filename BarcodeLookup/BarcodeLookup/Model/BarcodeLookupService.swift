//
//  BarcodeLookupService.swift
//  BarcodeLookup
//
//  Created by John Douglas on 20/02/18.
//  Copyright © 2018 John Douglas. All rights reserved.
//

import Foundation

protocol BarcodeLookupService {
    func lookup(barcode: String, onSuccess: @escaping (_ response: BarcodeLookupResponse) -> (), onError: @escaping (_ error: NetworkerError) -> ()) // in the ideal world, the actual representation of the response should be hidden and more 'friendly' use-case (read: abstract) objects returned.
}

class BarcodeLookupServiceImpl: BarcodeLookupService {

    let networker = JSONNetworker()
    
    func lookup(barcode: String, onSuccess: @escaping (_ response: BarcodeLookupResponse) -> (), onError: @escaping (_ error: NetworkerError) -> ()) {
        let request = formUrlRequest(for: barcode)
        
        networker.fire(request: request, onSuccess: { (jsonResponse) in
            do {
                let jsonDecoder = JSONDecoder()
                let response = try jsonDecoder.decode(BarcodeLookupResponse.self, from: jsonResponse.data(using: String.Encoding.utf8)!) // yes, its a hack to pass it as a string then immediately convert it back to Data. a better solution would be to pass the BarcodeLookupResponse class type down into the Json Networker with some light generics so it can scale with more responses.
                onSuccess(response)
            } catch {
                onError(NetworkerError.unknownResponseFormat) //i.e. some sort of {"error": "no item found"} response.
            }
        }, onError: onError)
    }
    
    private func formUrlRequest(for barcode: String) -> URLRequest {
        let endpoint = "https://www.datakick.org/api/items/\(barcode)"
        let url = URL(string: endpoint)!
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = "GET"
        return urlRequest
    }

}
