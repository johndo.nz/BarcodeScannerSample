//
//  BarcodeLookupResponse.swift
//  BarcodeLookup
//
//  Created by John Douglas on 20/02/18.
//  Copyright © 2018 John Douglas. All rights reserved.
//

import Foundation

struct BarcodeLookupResponse: Codable {
    let name: String
    let brand: String
    enum CodingKeys: String, CodingKey {
        case name
        case brand="brand_name"
    }
}
