//
//  JSONNetworker.swift
//  BarcodeLookup
//
//  Created by John Douglas on 20/02/18.
//  Copyright © 2018 John Douglas. All rights reserved.
//

import Foundation

enum NetworkerError: Error {
    case unexpectedHTTPStatusCode
    case transportError
    case malformedResponse
    case unknownResponseFormat // I could subjectively call this a hack. it sits more at the Model level, not in this low-level detail.
}

class JSONNetworker {
    
    private let urlSession = URLSession.shared
    
    func fire(request: URLRequest, onSuccess: @escaping (_ jsonString: String) -> (), onError: @escaping (_ error: NetworkerError) -> () ) {
        urlSession.dataTask(with: request) { (data, response, error) in
            if error != nil {
                onError(NetworkerError.transportError)
                return
            }
            
            if let response = response as? HTTPURLResponse {
                switch(response.statusCode) {
                case 100...199, 300...:
                    onError(NetworkerError.unexpectedHTTPStatusCode)
                    return
                case 200:
                    break;
                default:
                    onError(NetworkerError.malformedResponse) // yes, debatable. would be talking about this with other engineers and potentially the PO.
                    return
                }
            }
            
            if let data = data, let jsonString = String(data: data, encoding: String.Encoding.utf8) {
                onSuccess(jsonString)
                return
            } else {
                onError(NetworkerError.malformedResponse) // also debatable. just conscious of time and don't want to get caught up in these details.
                return
            }
        }.resume()
    }
    
}
