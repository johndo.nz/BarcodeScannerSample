# Sample Barcode Scanner

this is a simple POC which serves as a sample of the code I can produce with a deadline on time.
This is based on the MVVM pattern, but with some go-to abstractions intra-architectural-distinction that I like to use. 

## How do I run it?

I use [Carthage](https://github.com/Carthage/Carthage) to manage the massive list of 1 dependency.
1.  To run it, make sure you have Carthage installed (follow the Carthage link for instructions on how to install Carthage)
2.  `cd` into the Project directory (i.e. the folder with a "Cartfile" in it). then run `carthage update --platform iOS`
3.  after it has built, change the development team and run on device!

## So, as all things in Software Engineering are about tradeoffs, what ones have you made?

### In the interest of saving time and staying pragmatic, I have pulled [this amazing Barcode Scanner framework](https://github.com/hyperoslo/BarcodeScanner) to help with the Vision API.
_I did this so I can focus more on architecture, and how components talk to each other. If you look through the commit history, you can see this evolve from a very simple ViewController._

### OMG WHERE ARE THE UNIT TESTS?!?!?
_Yes, valid criticism. Although I have not included any, as my components communicate through interfaces, creating some is merely a time-exercise. I did not have the luxury to do this._

### What is your opinion on code comments?
_I do not like code comments. A comment represents a failure to express intent. I have mainly used it in this sample code as grounds for what my next steps would be for cleaning up. The comments commited do not necessarily reflect all of the things I would clean up either._
_however, code comments may be useful when working with a framework or library to help express the intent where the author has not managed to. For the next engineer that has to dig around in the integration code, it may be useful. of course, comments age and die, but I digress._

### What's going on with the INFO_DESCRIPTION_LABEL string?
_good question. I have not managed to find out exactly how the author wants you to set that informational string, and I am yet to spend time to figure out how the internal state machine behaves and what I have done wrong. In the interest of time, I have left as-is.mostly._

### What barcodes work with this app?
_also a good question. I am consuming the [Datakick](https://www.datakick.org) API for my Barcode lookup service. I chose this as I didn't have to mess around with credit-cards, accounts, registration, or API auth. **the tradeoff being that the API spec (from the samples I've seen) does not actually return a price, or description.**_
_to counteract this, I have substituted some other fields - a product name, and product brand. I hope this is OK, and normally I wouldn't be diverging from spec. Again, in the interest of time and getting something going, I made a decision to go with Datakick instead of spending more time investigating other services_

### so, what barcodes actually work?
_as mentioned above, I am consuming Datakick, which is somewhat incomplete in terms of its dataset. For testing the application, I have been using **[This product](https://www.datakick.org/805002000375)** which has a picture of the Barcode._
_you're welcome to scour the items through the website, but I clicked on a good handful, and none of them had images._

### what commit process/branching strategy do you use?
_trick question. for this small application and in the interest of time. I just commited to master at what I felt was logical increments._
_In production systems, I am a huge fan of [Gitflow](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow). Gitflow + PR = awesome, quality, stable VCS. In fact, for my self-employed projects, I follow gitflow and also review my own code._

Cheers! enjoy.